
# CapData Opera - Drupal - Documentation

## Présentation du Module CapData RDF

CapData RDF est un composant clé du projet CapData Opéra - France 2030, initié par la Réunion des Opéras de France dans le cadre du programme France 2030. Ce module vise à faciliter l'alignement et l'exposition des données au format RDF, essentiel pour l'interopérabilité et le partage des données culturelles.

Le module permet de préparer les données d'un site web pour l'export RDF basé sur l'ontologie CapDataOpera : [Ontologie CapDataCulture v1](https://ontologie.capdataculture.fr/v1/owl/).

### Fonctionnalités du module CapData RDF

#### Gestion Simplifiée des Alignements
- Interface utilisateur intuitive pour gérer facilement les alignements de modèle.
- Permet aux utilisateurs d'aligner leur modèle de données sans nécessiter de compétences techniques approfondies.

#### Module Avancé pour Alignements Complexes
- Outils pour gérer des alignements de données plus complexes.
- Supporte des scénarios d'intégration de données sophistiqués et personnalisés.

### Ressources Utiles
- [Site du projet CapData Opéra](https://www.rof.fr/rof/capdata-opera.aspx)
- [Guide pratique pour la publication des données](https://gitlab.com/capdataopera/capdata-doc/-/blob/branch/default/Guide_Pratique.md)
- [Documentation pour exposer les données](https://gitlab.com/capdataopera/capdata-doc/-/blob/branch/default/expose-data.md)
- [Ontologie CapDataCulture v1](https://ontologie.capdataculture.fr/v1/)

## Soutien et Financement

Cette opération est soutenue par l'État dans le cadre du dispositif « Expérience augmentée du spectacle vivant » de France 2030, et est opérée par la Caisse des Dépôts.

## Objectifs du Projet
- **Interopérabilité** : Faciliter le partage et l'exploitation des données culturelles entre différentes institutions.
- **Accessibilité** : Rendre les données culturelles plus accessibles et utilisables via des formats standards comme RDF.
- **Innovation** : Promouvoir l'innovation dans le secteur culturel en permettant une meilleure exploitation des données grâce aux technologies du Web sémantique.

Ce projet représente une avancée significative pour la gestion et l'utilisation des données culturelles en France, offrant des outils robustes pour l'alignement et l'exposition des données au format RDF.

## Processus de Mise en Œuvre
1. Sélection des jeux de données à exposer et choix de la licence appliquée pour leur réutilisation.
2. Téléchargement et intégration du module prêt à l’emploi.
3. Paramétrage des alignements par le personnel en charge du site web, sans nécessiter de connaissances en développement informatique.
4. Inscription au service CapData et sélection des traitements, enrichissements et expositions des données.

## Exemples d'Utilisation
- **Communication** : Le service de communication d'une maison d'opéra peut enrichir et diffuser les données sur le site internet de l’établissement ou d’un office de tourisme.
- **Collectivités Territoriales** : Peuvent collecter et diffuser la programmation d’une maison d’opéra via différents supports sans saisie supplémentaire.
- **Artistes** : Accès à la liste des contenus et médias auxquels ils ont participé, et aux audiences liées.

## Modèle de données CapData Opéra
Le modèle de données couvre des éléments tels que l'œuvre, la personne, le lieu, la production (spectacle) et les événements, assurant ainsi une gestion et une exposition cohérentes et complètes des données culturelles.

## Conclusion
Le module CapData RDF est conçu pour répondre aux besoins des établissements culturels en matière de gestion et de valorisation des données, offrant une solution agile et interopérable, soutenue par une gouvernance collaborative et une approche écoresponsable.

Pour plus de détails, visitez la [page projet CapData Opéra](https://www.rof.fr/rof/capdata-opera.aspx).

### Mentions
- **Conception et pilotage** : Réunion des Opéras de France
- **Développement** : Bluedrop
- **Accompagnement et architecture** : Logilab
- **Soutien et financement** : Cette opération est soutenue par l'État français dans le cadre du dispositif « Expérience augmentée du spectacle vivant » de France 2030 et est opérée par la Caisse des Dépôts.

## Personnalisation pour les Alignements Complexes au Modèle

### Créer un Module Drupal
1. **Créer le répertoire du module** : Créez un répertoire pour votre module dans le dossier `modules/custom` de votre installation Drupal, par exemple `modules/custom/rof_capdata`.
2. **Créer le fichier d'info du module** : Créez un fichier `.info.yml` pour votre module, par exemple `rof_capdata.info.yml`.

### Créer le Fichier du Module
Créez un fichier `.module` dans le même répertoire, par exemple `rof_capdata.module`, et placez-y votre code. La variable $host sera remplie automatiquement à partir de la configuration du module.

### Activer le Module
Pour activer votre module, allez dans la page d'administration des modules de Drupal (`admin/modules`), cochez la case à côté de votre module "ROF CapData", puis cliquez sur "Enregistrer la configuration".

### Génération du Fichier d'Export
```bash
drush capdata-rdf-export --uri=https://opera-exemple.com
```

### Utilisation des Hooks
Les hooks définis dans le fichier `.module` sont automatiquement détectés et utilisés par Drupal si les modules nécessaires et les conditions sont remplies. Dans ce cas, les hooks `hook_capdata_classes_info_alter`, `hook_capdata_graph_beginning_alter`, et `hook_capdata_graph_item_alter` modifient le comportement des exports de CapData en fonction des besoins spécifiques définis dans le code.

**Focus** : `hook_capdata_graph_alter` est aussi utile pour l'ajout de données supplémentaires au graphe à la fin de l'export.

## Conclusion
En suivant ces étapes, vous intégrerez votre code PHP dans un module Drupal personnalisé. Les hooks définis seront alors automatiquement invoqués par Drupal au moment approprié, permettant d'altérer et d'ajouter des fonctionnalités spécifiques aux exportations de CapData.
